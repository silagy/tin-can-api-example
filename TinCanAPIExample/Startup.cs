﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TinCanAPIExample.Startup))]
namespace TinCanAPIExample
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
